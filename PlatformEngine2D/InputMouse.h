#pragma once

#include <irrlicht.h>

using namespace irr;

class EventReceiverMgr;

class InputMouse
{
public:

	friend class EventReceiverMgr;

	static void Reset()
	{
		LeftKeyIsDown = false;
		AsyncLeftKeyPressed = false;
		AsyncLeftKeyReleased = false;
		SyncLeftKeyPressed = false;
		SyncLeftKeyReleased = false;
	}

	static core::position2di GetPosition()
	{
		return Position;
	}

	static core::position2di GetPressedPosition()
	{
		return PressedPosition;
	}

	static core::position2di GetReleasedPosition()
	{
		return ReleasedPosition;
	}

	///This is used to check whether a key is being held down
	static bool IsLeftKeyDown()		{ return LeftKeyIsDown; }
	static bool IsRightKeyDown()	{ return RightKeyIsDown; }


	static bool IsLeftKeyPressed( )		{ return SyncLeftKeyPressed; }
	static bool IsLeftKeyReleased( )	{ return SyncLeftKeyReleased; }
	static bool IsRightKeyPressed( )	{ return SyncRightKeyPressed; }
	static bool IsRightKeyReleased( )	{ return SyncRightKeyReleased; }

	// Call every update
	static void Update( )
	{
		//left key
		SyncLeftKeyPressed = AsyncLeftKeyPressed;
		AsyncLeftKeyPressed = false;
		SyncLeftKeyReleased = AsyncLeftKeyReleased;
		AsyncLeftKeyReleased = false;
		//right key
		SyncRightKeyPressed = AsyncRightKeyPressed;
		AsyncRightKeyPressed = false;
		SyncRightKeyReleased = AsyncRightKeyReleased;
		AsyncRightKeyReleased = false;

		if( SyncLeftKeyPressed )
		{
			PressedPosition = Position;
		}

		if( SyncLeftKeyReleased )
		{
			ReleasedPosition = Position;
		}
	}

private:

	static core::position2di Position;
	static core::position2di PressedPosition;
	static core::position2di ReleasedPosition;

	// We use this array to store the current state of each key
	static bool LeftKeyIsDown;
	static bool RightKeyIsDown;

	// Keep pressed state
	static bool AsyncLeftKeyPressed;
	static bool AsyncRightKeyPressed;

	// keep released state
	static bool AsyncLeftKeyReleased;
	static bool AsyncRightKeyReleased;

	// Sync state
	static bool SyncLeftKeyPressed;
	static bool SyncLeftKeyReleased;
	static bool SyncRightKeyPressed;
	static bool SyncRightKeyReleased;
};