#include "InputMouse.h"

core::position2di InputMouse::Position			= core::vector2di(0);
core::position2di InputMouse::PressedPosition	= core::vector2di(0);
core::position2di InputMouse::ReleasedPosition	= core::vector2di(0);

// We use this array to store the current state of each key
bool InputMouse::LeftKeyIsDown	= false;
bool InputMouse::RightKeyIsDown = false;

// Keep pressed state
bool InputMouse::AsyncLeftKeyPressed	= false;
bool InputMouse::AsyncRightKeyPressed	= false;

// keep released state
bool InputMouse::AsyncLeftKeyReleased	= false;
bool InputMouse::AsyncRightKeyReleased	= false;

// Sync state
bool InputMouse::SyncLeftKeyPressed		= false;
bool InputMouse::SyncLeftKeyReleased	= false;
bool InputMouse::SyncRightKeyPressed	= false;
bool InputMouse::SyncRightKeyReleased	= false;